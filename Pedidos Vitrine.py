
# coding: utf-8

# In[100]:


import http.client
import json

for rl in range(1,7):
    print(rl)
    appKey = ["","vtexappkey-lojaanimale-NPKDKY","vtexappkey-lojafarm-NTPFQN","vtexappkey-lojaabrand-FQRPLO","vtexappkey-lojafabula-PVGKGT","vtexappkey-lojafyi-ZEMYKL","vtexappkey-foxton-AQYHKM"]
    appToken = ["","XXPSGLOUYYYCAMONDCOBDAXULAKGTOGBJQWYVESMLFUYDQXILTWJRWZWAVGGAIBJRXTKPPRZJBQXGYQUCCUZRWKMQZYAXMMCDKGGXNSVBXAPGCQBELDXNCSUMHTPRRBX","TSSMNSVVLFRXAJALLDRFUJWBQGNWOZKNPSMSCMPVZAGMPYIJJMFDAFYJWQVHFYHMVEIHXIIZOVGUDKLSXDNBTGMJUCWLQHQBTUQLPALBEGPEZDGLFCJBKFSORZMVLWIG","VXYYRPOREYZDXKKKSVZBGXPQLBHFWMQGJBEFHVUUFBEGIFAYBDOONWEXIHMPHJVSISUWFFCWBEVFSIIFBIDSXBTMXGWAIEMVFMXPLETVBSNIAYEZZHOZFCVGZIYYICOX","QZLDYMNGDUBLBYZGIAZIQQYRGODEHFRAOYOBBGPPKUNDXWWPVCWYIBJZAUWJIIYYDAWDKXECLJXVDVTKSOJFJSSMPOAHKGHBVOAXZSFXXUNYDNWFAMQTZTOKHKOVWXRE","WJLQYRDFGDAKAFICEUHZTWDJYXHULUCRSDGZEJMMUVJUXISULXXOGBYAFCSAKITLBJSWZJYVIVEXMJLPFMJOWNKRFFTBRMBMBYSOQVVCYCGLWANWQCUYZFPNYARTPXDV","KNCQKFUHJMZSUOEVDCPVHFIGFOKAITXTWNAQGARROSRBXHJJCJMTIHPFFWKWVTZPOGVHNOOCKYHONGIXEBYYHDKPYVLKPVWUTCNQMCWNVJJKZARYNNRUUAHLWFIRDDNP"]
    amb = ["","lojaanimale","lojafarm","lojaabrand","lojafabula","lojafyi","foxton"]

    conn = http.client.HTTPSConnection(amb[rl]+".vtexcommercestable.com.br")

    headers = {
        'accept': "application/json",
        'content-type': "application/json",
        'x-vtex-api-appkey': appKey[rl],
        'x-vtex-api-apptoken': appToken[rl],
        'cache-control': "no-cache",
        'postman-token': "88c7ad17-496e-1350-591d-e56d6f401705"
         }


    conn.request("GET", "/api/oms/pvt/orders?f_status=waiting-ffmt-authorization", headers=headers)

    res = conn.getresponse()
    data = res.read()
    data = data.decode("utf-8")
    lista = json.loads(data)
    lista_todos_pedidos = lista ['list']
    lista_pedidos_vitrine = []
    for item in lista_todos_pedidos:
        pedido = item['orderId']
        if pedido.startswith('1-'):
            lista_pedidos_vitrine.append(pedido)

    print(lista_pedidos_vitrine,'Lista de pedidos com pendência de vitrine')

    for Pedido in lista_pedidos_vitrine:
        conn.request("POST", "/api/oms/pvt/orders/"+Pedido+"/changestate/authorize-fulfillment", headers=headers)

        res = conn.getresponse()
        data = res.read()

        print(data.decode("utf-8"))
        print("Alterando status do pedido", Pedido)

    print("Fim!")

